#!/usr/bin/env python3

import socket, sys, string

from select import select

from _thread import *

from hashlib import md5
from base64 import b64encode, b64decode

from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

SERVER_MODE = True

HOST = ''
PORT = 8888

TIMEOUT = 30

PASSWORD = 'SUPERSECRETPASSWORDKEEPAWAY!'

INTRO = """
Welcome! The Oracle will see you now!

"""

MENU = """
1) Check padding
2) Enter password

"""

OUTRO = """
Congratulations!

Here's your flag:

"""

FLAG = "TUCTF{D0nt_l3t_y0ur_s3rv3r_g1v3_f33db4ck}"

CHECK_PAD_PROMPT = "\nGive me your input: "

ENTER_PASSWORD_PROMPT = "\nWhat is the password? "


class AESCipher:
    def __init__(self, key):
        self.key = md5(key).digest()

    def encrypt(self, data):
        iv = get_random_bytes(AES.block_size)
        self.cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return b64encode(iv + self.cipher.encrypt(pad(data.encode('utf-8'),
                AES.block_size))).decode('utf-8')

    def decrypt(self, data):
        raw = b64decode(data)
        self.cipher = AES.new(self.key, AES.MODE_CBC, raw[:AES.block_size])
        return self.cipher.decrypt(raw[AES.block_size:])

def timeoutfunc():
    sys.stdout.write('\nYou idled too long\n\n')
    sys.stdout.flush()
    sys.exit(0)

def handle_check_pad(conn, aes):
    if conn is None:
        sys.stdout.write(CHECK_PAD_PROMPT)
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n')
        try:
            decrypted = aes.decrypt(data)
            sys.stdout.write('\nPadding Valid\n')
            sys.stdout.flush()
        except ValueError:
            sys.stdout.write('\nPadding Invalid\n')
            sys.stdout.flush()
    else:
        conn.send(CHECK_PAD_PROMPT.encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n')

        decrypted = aes.decrypt(data)

        try:
            unpad(decrypted, AES.block_size)
            conn.send('\nPadding Valid\n'.encode('utf-8'))
            sys.stdout.flush()
        except ValueError:
            conn.send('\nPadding Invalid\n'.encode('utf-8'))

def handle_enter_password(conn):
    if conn is None:
        sys.stdout.write(ENTER_PASSWORD_PROMPT)
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n')
        if data == PASSWORD:
            sys.stdout.write('\nThat\'s it!\n')
            sys.stdout.flush()
            return True
        else:
            sys.stdout.write('\nThat\'s incorrect\n')
            sys.stdout.flush()
            return False
    else:
        conn.send(ENTER_PASSWORD_PROMPT.encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n')

        if data == PASSWORD:
            conn.send('\nThat\'s it!\n'.encode('utf-8'))
            return True
        else:
            conn.send('\nThat\'s incorrect\n'.encode('utf-8'))
            return False

def handle_menu(conn, aes):
    if conn is None:
        sys.stdout.write(f'\nMENU:\n{MENU}')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        try:
            choice = int(data.rstrip('\n'))
            if choice == 1:
                handle_check_pad(conn, aes)
                return False
            elif choice == 2:
                return handle_enter_password(conn)
            else:
                sys.stdout.write('Not a valid option\n')
                sys.stdout.flush()
                return False
        except ValueError:
            sys.stdout.write('Input needs to be an integer\n')
            sys.stdout.flush()
            return False
    else:
        conn.send((f'\nMENU:\n{MENU}').encode('utf-8'))

        data = conn.recv(16)
        data = data.decode('utf-8').rstrip('\n')

        try:
            choice = int(data)
            if choice == 1:
                handle_check_pad(conn, aes)
                return False
            elif choice == 2:
                return handle_enter_password(conn)
            else:
                conn.send('Not a valid option\n'.encode('utf-8'))
                return False
        except ValueError:
            conn.send('Input needs to be an integer\n'.encode('utf-8'))
            return False

if SERVER_MODE:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')

    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print('Bind failed. Error code: ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()

    print('Socket bind complete')

    s.listen(10)
    print('Socket now listening')

def clienthread(conn):
    try:
        aes = AESCipher(get_random_bytes(AES.block_size))

        ciphertext = aes.encrypt(PASSWORD)

        conn.send((f'{INTRO}\nYour ciphertext is:\n\n{ciphertext}\n').encode('utf-8'))

        got_password = False
        while not got_password:
            got_password = handle_menu(conn, aes)

        conn.send((f'{OUTRO}\n{FLAG}\n\n').encode('utf-8'))

        conn.close()
    except (BrokenPipeError, ConnectionResetError):
        pass
    except socket.timeout:
        conn.send('\nYou idled too long.\n'.encode('utf-8'))

cont = True
while cont:
    if SERVER_MODE:
        try:
            conn, addr = s.accept()
            print('Connected with ' + addr[0] + ': ' + str(addr[1]))
            conn.settimeout(TIMEOUT)

            start_new_thread(clienthread, (conn,))
        except KeyboardInterrupt:
            sys.stdout.write('Shutting down\n')
            sys.stdout.flush()
            sys.exit(0)
    else:
        try:
            aes = AESCipher(get_random_bytes(AES.block_size))

            ciphertext = aes.encrypt(PASSWORD)

            sys.stdout.write(f'{INTRO}\nYour ciphertext is:\n\n{ciphertext}\n')
            sys.stdout.flush()
            conn = None

            got_password = False
            while not got_password:
                got_password = handle_menu(conn, aes)

            sys.stdout.write(f'{OUTRO}\n{FLAG}\n\n')
            sys.stdout.flush()

            cont = False
        except KeyboardInterrupt:
            sys.stdout.write('\n')
            sys.stdout.flush()
            sys.exit(0)

if SERVER_MODE:
    s.close()
