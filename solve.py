#!/usr/bin/env python2

from pwn import *

from base64 import b64encode, b64decode

# TODO change address
r = remote('127.0.0.1', 8888)

r.readuntil('is:\n\n')

ciphertext = b64decode(r.readline().rstrip('\n'))

plaintext = ''

# for i in xrange(len(ciphertext), 0, -1):
blocks = 2
pad_chars = ''
for i in xrange(blocks, 0, -1):
    for j in xrange(15, -1, -1):
        for ci in xrange(255):
            pad = 16 - j
            new_ciphertext = ciphertext[:i*16-pad] + chr(ci) + pad_chars + ciphertext[i*16:]
            assert len(new_ciphertext) == len(ciphertext)
            r.readuntil('password\n\n')
            r.sendline('1')
            r.readuntil('input: ')
            r.sendline(b64encode(new_ciphertext))
            r.readuntil('Padding ')
            response = r.readline()
            if 'Valid' in response:
                c = ciphertext[i*16-pad]
                # print 'valid'
                if ci != c:
                    plaintext = chr((ord(c)^ci)^pad) + plaintext
                    # print ord(c)^ci^pad
                    pad_chars = ''
                    # print pad ^ (pad + 1)
                    for ck in new_ciphertext[i*16-pad:i*16]:
                        pad_chars += chr(ord(ck)^(pad ^ (pad + 1)))
                    # print [ord(ii) for ii in pad_chars]
                    # print list(plaintext)
                    break
    ciphertext = ciphertext[:i*16]
    pad_chars = ''

print plaintext

print ord(ciphertext[len(ciphertext)-1])

r.close()

# print ciphertext
