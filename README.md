# The Oracle

Desc: `The oracle knows all, and it's kinda chatty. Is it telling you something?`

Flag: `TUCTF{D0nt_l3t_y0ur_s3rv3r_g1v3_f33db4ck}`
