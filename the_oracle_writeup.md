# The Oracle -- TUCTF2019

Recognizing that the challenge says "Oracle" and that it checks padding should make it clear that the challenge is a padding oracle attack. There are resources online for learning more about the theory behind it. For application, you can study my script.

Solve Script: `solve.py`

Flag: `TUCTF{D0nt_l3t_y0ur_s3rv3r_g1v3_f33db4ck}`
